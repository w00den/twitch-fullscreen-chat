// ==UserScript==
// @name         Twitch fullscreen chat
// @namespace    w00den-twitch-fullscreen-chat
// @version      1.0.0
// @description  chat window in fullscreen which you can scale, drag, resize
// @author       w00den@protonmail.com
// @icon         https://www.twitch.tv/favicon.ico
// @homepageURL  https://gitlab.com/w00den/twitch-fullscreen-chat
// @supportURL   https://gitlab.com/w00den/twitch-fullscreen-chat/issues
// @updateURL    https://gitlab.com/w00den/twitch-fullscreen-chat/raw/master/tfc.update.js
// @downloadURL  https://gitlab.com/w00den/twitch-fullscreen-chat/raw/master/tfc.update.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.0.0/polyfill.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/cssobj/1.3.6/cssobj.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/loadCSS/2.0.1/loadCSS.min.js
// @include      https://twitch.tv/*
// @include      https://www.twitch.tv/*
// @run-at       document-start
// ==/UserScript==

/* jshint ignore:start */
var inline_src = (<><![CDATA[
/* jshint ignore:end */
/* jshint esnext: false */
/* jshint esversion: 6 */

class TFC {
  constructor(props) {
    props = props || {}
    this.debug = !!props.debug
    this.fs = false
    this.chatEl = null
    this.cssObj = null
    this.BGOpacity = 0
    this.chatContainer = $('<div class="floating-chat-container"></div>')
    this.css = {
      '.tfc_fs .right-column': {
        position: 'absolute!important',
        right: 0,
        zIndex: 1
      },
      ' .tfc_fs .chat-line__message, .tfc_fs .vod-message': {
        background: 'none!important'
      },
      '.tfc_fs .video-watch-page__right-column, .tfc_fs .channel-root__right-column, .tfc_fs .chat-room, .tfc_fs .chat-list, .tfc_fs .chat-input': {
        backgroundColor: 'rgba(0, 0, 0, 0)'
      },
      '.tfc_fs .chat-list,.tfc_fs .right-column:hover .chat-input, .tfc_fs .video-chat__message-list-wrapper, .tfc_fs .right-column:hover .video-chat__input': {
        backgroundColor: 'rgba(0, 0, 0, 0)'
      },
      '.tfc_fs .video-chat__header, .tfc_fs .room-selector__header, .tfc_fs .pinned-cheer-v2, .chat-room__notifications': {
        display: 'none!important'
      },
      '.tfc_fs .vod-message__header, .tfc_fs .chat-line__timestamp': {
        textShadow: 'none',
        // display: 'none!important'
      },
      '.tfc_fs .video-cha,.tfc_fs .tw-border-l': {
        border: '0!important',
        // flexBasis: 'calc(34rem - 51.7031px)'
      },
      '.tfc_fs .tw-c-text-alt': {
        color: '#484848!important'
      },
      '.tfc_fs .tw-c-text-base': {
        color: '#e4e4e4!important'
      },
      '.tfc_fs .chat-line__message, .tfc_fs .vod-message': {
        padding: '.5rem 2rem',
        textShadow: '0 0 5px #010000, 0 0 5px #010000, 0 0 5px #010000, 0 0 15px #010000, 0 0 15px #010000'
      },
      '.tfc_fs .video-watch-page__right-column, .tfc_fs .channel-root__right-column': {
        // width: 'calc(34rem - 51.7031px)'
      },
      '.tfc_fs .video-chat__input, .tfc_fs .chat-input': {
        opacity: 0,
        position: 'relative',
        transition: 'all ease .5s'
      },
      '.tfc_fs .video-chat__input textarea, .tfc_fs .chat-input textarea': {
        background: 'rgba(0, 0, 0, 0.59)',
        border: '1px solid #464646',
        color: '#fff'
      },
      '.tfc_fs .video-chat:hover .video-chat__input, .tfc_fs .right-column:hover .chat-input': {
        opacity: 1
      },
      '.floating-chat-container': {
        display: 'none',
        zIndex: 1
      },
      '.tfc_fs .floating-chat-container': {
        display: 'block',
        position: 'absolute!important',
        right: 0,
        top: '50px',
        bottom: '70px',
        width: 'fit-content'
      },
      '.tfc_fs .video-watch-page__right-column': {
        textShadow: '0px 0px 0.2em #000'
      },
      '.tfc-controls, .tfc-scale-up, .tfc-scale-down': {
        display: 'none'
      },
      '.tfc_fs .tfc-controls': {
        display: 'flex',
        flexDirection: 'column',
        position: 'absolute',
        top: '4px',
        right: 0
      },
      '.tfc_fs .tfc-control': {
        display: 'block',
        width: '14px',
        height: '14px',
        margin: '3px',
        background: 'rgba(0, 0, 0, .5)',
        borderRadius: '50%',
        position: 'relative',
        boxShadow: '0 0 4px 2px #8c5dd8',
        cursor: 'pointer'
      },
      '.tfc-control:after': {
        content: "''",
        position: 'absolute',
        top: 0,
        left: 0,
        color: '#a886e0',
        display: 'block',
        width: '14px',
        height: '14px',
        fontSize: '16px',
        textAlign: 'center',
        fontWeight: 'bold',
        lineHeight: '14px',
        verticalAlign: 'middle'
      },
      '.tfc-scale-up:after': {
        content: "'\\002b'"
      },
      '.tfc-scale-down::after': {
        content: "'\\002d'",
        top: '-1px'
      },
      '.tfc-toggle-bg::after': {
        content: "'\\2234'"
      }
    }
    this.ready = setInterval(this.tryInit, 100)
  }

  tryInit = () => {
    if (!$('.right-column').length) {
      return false
    }
    if (!$('.chat-input').length && !$('.video-chat__input').length) {
      return false
    }
    if (!$('.video-player__container').length) {
      return false
    }

    clearInterval(this.ready)
    this.init()
  }

  init = () => {
    loadCSS('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css')
    this.cssObj = cssobj(this.css)
    this.chatEl = $('.right-column')
    this.chatContainer
      .prependTo($('.video-player__container'))
      .draggable()
      .resizable({
        handles: 'se',
        stop: function(event, ui) {
          $(this).css("width", '')
        }
      })
      .css({height: $('.right-column').height() + 'px'})
    const controlsContainer = $('<div class="tfc-controls"></div>')
    const chatInput = $('.chat-input').length
      ? $('.chat-input')
      : $('.video-chat__input')
    controlsContainer.appendTo(chatInput)

    const btnScaleUp = $('<div class="tfc-control tfc-scale-up"></div>')
    btnScaleUp.on('click', this.scaleChatUp)
    btnScaleUp.appendTo(controlsContainer)

    const btnScaleDown = $('<div class="tfc-control tfc-scale-down"></div>')
    btnScaleDown.on('click', this.scaleChatDown)
    btnScaleDown.appendTo(controlsContainer)

    const btnToggleBG = $('<div class="tfc-control tfc-toggle-bg"></div>')
    btnToggleBG.on('click', this.toggleBG)
    btnToggleBG.appendTo(controlsContainer)

    this.checkInterval = setInterval(this.checkFS, 200)
    this.debug && console.log('[TFC] Ready')
  }

  toggleBG = () => {
    this.BGOpacity = this.BGOpacity >= 1
      ? 0
      : this.BGOpacity + .25
    const textShadow = this.BGOpacity > .5
      ? 'none'
      : '0 0 5px #010000, 0 0 5px #010000, 0 0 5px #010000, 0 0 15px #010000, 0 0 15px #010000'

    const toUpdate = {
      '.tfc_fs .chat-list,.tfc_fs .right-column:hover .chat-input, .tfc_fs .video-chat__message-list-wrapper, .tfc_fs .right-column:hover .video-chat__input': {
        ...this.css['.tfc_fs .chat-list,.tfc_fs .right-column:hover .chat-input, .tfc_fs .video-chat__message-list-wrapper, .tfc_fs .right-column:hover .video-chat__input'],
        backgroundColor: 'rgba(15, 14, 17, ' + this.BGOpacity + ')'
      },
      '.tfc_fs .chat-line__message, .tfc_fs .vod-message': {
        ...this.css['.tfc_fs .chat-line__message, .tfc_fs .vod-message'],
        textShadow
      }
    }
    this.debug && console.log('[TFC] bg opacity: ' + this.BGOpacity)
    this.updateCSS(toUpdate)
  }

  scaleChatUp = (event) => {
    this.scaleChat(.10, event)
  }

  scaleChatDown = (event) => {
    this.scaleChat(-.10, event)
  }

  scaleChat = (scaleBy, event) => {
    const targetEl = $('.right-column')
    const currentScale = targetEl[0].getBoundingClientRect().width / targetEl[0].offsetWidth
    const nextScale = currentScale + scaleBy
    this.debug && console.log('[TFC] scale: ' + nextScale)


    const toUpdate = {
      '.floating-chat-container': {
        ...this.css['.floating-chat-container'],
        transform: 'scale(' + nextScale + ')'
      }
    }
    this.updateCSS(toUpdate)

    this.chatContainer.draggable({
      start: event => {
        this.dragClickX = event.clientX
        this.dragClickY = event.clientY
      },
      // HACK: fix zoom drag bug
      drag: (event, ui) => {
        const original = ui.originalPosition
        ui.position = {
          left: (event.clientX - this.dragClickX + original.left),
          top: (event.clientY - this.dragClickY + original.top) + (ui.helper.height() * nextScale - ui.helper.height()) / 2
        }
      }
    })
  }

  updateCSS = (obj) => {
    this.css = {
      ...this.css,
      ...obj
    }
    this.cssObj.update(this.css)
  }

  checkFS = () => {
    const target = $('.video-player__container')
    const state = target.parent().hasClass('video-player--fullscreen')
    if (this.fs != state) {
      this.fs = state
      if (state) {
        this.handleEnterFS()
      } else {
        this.handleExitFS()
      }
    }
  }

  handleEnterFS = () => {
    this.debug && console.log('[TFC] entered fullscreen')
    $('html').addClass('tfc_fs')
    this.chatEl.appendTo(this.chatContainer)
    $('.video-chat__sync-button').click()
  }

  handleExitFS = () => {
    this.debug && console.log('[TFC] exited fullscreen')
    $('html').removeClass('tfc_fs')
    this.chatEl.insertAfter($('main'))
    $('.video-chat__sync-button').click()
  }
}

window.TFC = new TFC({debug: true})



/* jshint ignore:start */
]]></>).toString();
var c = Babel.transform(inline_src, {presets: ['stage-2', 'es2016']}, {sourcemaps: true});
eval(c.code);
/* jshint ignore:end */